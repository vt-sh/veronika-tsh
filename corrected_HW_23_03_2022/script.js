 // task_1 

function uncompress(str = "") {
    const length = str.length;
    let i = 0;
    let newStr = '';
    while(i < length) {
        let count = +str[ i + 1]; // кол-во букв
        let letter = str[i]; // буква
        if( Number.isInteger(count)) {
            newStr = newStr.padEnd(newStr.length + count, letter);
            i = i + 2;
        }else {
            newStr += letter;
            i++;
        }
    }
    return newStr;
}

console.log(uncompress('ab3cd4')); 

// task_3

function getFibonacci(n) {
     if(!n) {
         return [0, 1];
     } else {
         let arr = getFibonacci(n -1);
         let length = arr.length;
         arr = [...arr, arr[length-1] + arr[length-2]];
         return arr;
     }
}


console.log(getFibonacci(5)); 



 