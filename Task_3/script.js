
import {HashStorage} from "./HashStorage.js";

window.addEventListener('DOMContentLoaded', () => {

const coctailsStorage = new HashStorage();
const form = document.querySelector("form"),
      divArr = form.querySelectorAll(".description");
let result;//результат ответа  
     
function toggleClass(id = '') { // функци переключения скрыть/ активировать блок 

    divArr.forEach((el) => {
        el.classList.add('hidden');
        el.classList.remove('active');
    });
    
    if(id) {
        let tag = document.querySelector(id);
        tag.classList.remove('hidden');
        tag.classList.add('active');
    }
   
}

form.addEventListener('click', (ev) => {
   /*  const request = new XMLHttpRequest;
    request.open('POST', "./script.js");
    request.setRequestHeader('Access-Control-Allow-Origin', '*');
    request.send();  
    */
    if ( !(ev.target.name  == "isАlcohol") ) {
        ev.preventDefault();   
    };
    
    switch(ev.target.value) {
        case "ввод рецепта" :
            toggleClass('#enterRecept');
            break; 
        case "рецепт напитка" : 
            toggleClass('#findRecip');
            break; 
        case "удаление рецепта":
            toggleClass('#deleteRecip');
            break; 
        case "перечень всех коктейлей": // получение всех коктелей
            toggleClass("#listRecipes");
            let div = document.querySelector("#listRecipes");
            div.innerHTML = "";
            coctailsStorage.getKeys().forEach( (el) => {
                result = coctailsStorage.getValue(el);
                if(result) {

                    div.innerHTML += `<div>Название: ${el} `;
                    for(let key in result) {

                    div.innerHTML += `${key} : ${result[key]} <br> `;
                    }
                    div.innerHTML += '</div><hr>';

                } else {
                    div.innerHTML = "Рецепты не найдены";
                }
             });

             console.log(div);
            break; 
        case "Отправить":
            
            let input = ev.target.parentElement.querySelectorAll("input");// все input в блоке
            
            switch(ev.target.parentElement.id) {
                case "enterRecept": // форма ввод рецепта
                    const textarea =  ev.target.parentElement.querySelectorAll("textarea");
                    if(textarea[0].value && textarea[1].value && input[0].value  && input[1].value) { // заполнил все поля
                     
                        let value = {"Ингридиенты и пропорции":textarea[0].value, "Рецепт приготовления": textarea[1].value };

                        (input[1].checked) ? value['Алкогольный'] = true :  value['Алкогольный'] = false ;
                        coctailsStorage.addValue( input[0].value , value);

                        textarea[0].value = textarea[1].value = "";
                        input[1].checked = input[2].checked = false;
                        input[0].value = "";
                        document.querySelector('#enterRecept .answer').innerHTML = "<br>Коктейль добавлен"
                    } else { // не заполнил поля
                        document.querySelector('#enterRecept .answer').innerHTML = "<br>Вы заполнили не все поля";
                        
                    }
                    break;
                case "findRecip": // форма найти рецепт
                    result = coctailsStorage.getValue(input[0].value);
                    document.querySelector("#findRecip .recipe").innerHTML = `Название: ${input[0].value} <br>`;

                    if(result) {
                        for(let key in result) {
                        document.querySelector("#findRecip .recipe").innerHTML += `${key} : ${result[key]} <br> `;
                        }
                    } else {
                        document.querySelector("#findRecip .recipe").innerHTML = "Рецепт не найден";
                    }
                    input[0].value = "";
                    
                    
                    break;
                case "deleteRecip": // форма удаление рецепта

                    document.querySelector("#deleteRecip .answer").innerHTML = "";
                    result = coctailsStorage.deleteValue(input[0].value);
                    result ? result = "Удален рецепт: " + input[0].value  : result = "Не найден рецепт: " + input[0].value;
                    document.querySelector("#deleteRecip .answer").innerHTML = result;
                    input[0].value = "";
                    break;
            }
            
            break; 

    }
   
});

let testRecept =  {"Ингридиенты и пропорции": "Водка Finlandia 50мл Кофейный ликер 25мл.  Лед в кубиках 120 г" ,"Рецепт приготовления": "Наполни стакан кубиками льда доверху, затем налей кофейный ликер 25 мл, водку 50 мл и размешай коктейльной ложкой." , "Алкогольный": true};
let name = "Маргарита";

let test2Recept = {"Ингридиенты и пропорции": "Гренадин Monin 10мл ,Клубничный сироп Monin 10мл, Персиковый сок 150мл, Лимонный сок 15мл, Банан 110г, Клубника 50г, Дробленый лед 60г" ,"Рецепт приготовления": "Положи в блендер очищенную и нарезанную половинку банана и клубнику 2 ягоды. Налей лимонный сок 15 мл, гренадин 10 мл, клубничный сироп 10 мл и персиковый сок 150 мл. Добавь в блендер совок дробленого льда и взбей. Перелей в хайбол. Укрась кружком банана и половинкой клубники на коктейльной шпажке." , "Алкогольный": false};
let name2 = "Пеликан";

coctailsStorage.addValue(name, testRecept);
coctailsStorage.addValue(name2, test2Recept);

console.log("Hello", coctailsStorage.storage)



}); 

