export class HashStorage{

    storage = {};
    addValue(key,value) { // сохр. key = value
        
        if(key && value) {
            key = key.toLowerCase();
            this.storage[key] = value;
            return true
        } else {
            return false;
        }
    } 

    getValue(key) {  // возвр. value по key или underfind 
        key = key.toLowerCase();
        if (this.storage[key] ) {
            return this.storage[key];
        } else {
            return undefined;
        } 
    }

    deleteValue(key) { // удаляет  value по key. TRUE == удал, False == небыло
        key = key.toLowerCase();
        if (this.storage[key] ) {
            delete this.storage[key];
            return true;
        } else {
            return false;
        } 

    } 
    getKeys() { // массив ключей

        return Object.keys(this.storage);
    } 
};





