//task_1 
function sum(a) {
 
    let currentSum = a;
 
    function f(b) {
      currentSum += b;
      return f;
    }
 
    f.toString = function() {
      return currentSum;
    };
 
    return f;
}
 
let result = sum(1)(2)(5);

result = "" + result;

// task_2 - Рекурсия уменьшает число n пока не станет однозначным 

 function rec(n) {
    let reg = /\d{1}/g;
    let arr = ("" + n).match(reg);
    let sum;

    sum = arr.reduce((prev,next) => {
        return ( +prev ) + ( +next );

    })

    if (sum < 10) {
       return sum;
    } else {
       return rec(sum);
    }   
}

let test = rec(123456); 


// task_3

function sortNumber(arr) {
    let arrOddNumber = [];
    arr.forEach( (el, i) => {
        if (el % 2 != 0) {
            arrOddNumber.push(el);
            delete arr[i];
        }
    });
    arrOddNumber.sort( (a,b) => {
        return a - b;
    });
   
    let count = 0;
 
    for (let i = 0; i < arr.length; i++) {
        console.log(i, arr[i]);
        if (arr[i] === undefined) {
            arr[i] = arrOddNumber[count];
            count++;
        }
    }
    return arr;
 
}
 
 
let result2 = sortNumber( [5, 8, 6, 3, 4] );


// task_4

function sortStr(str) {
    str = str.split('');
    str.sort();
    return str.join('');
}
 
function findAnagrams(str = "", arrStr = []) {
    str = sortStr(str);
   
    return arrStr.filter((el) => {
               el = sortStr(el);
               if(el == str) {
                   return el;
                }  
            })  ;
 
};
 
let result3 = findAnagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']);









 